This repository illustrates how test coverage statistics can be displayed for state machines in Bloqqi. The file ```StateMachine.dia``` defines a state machine in Bloqqi and the file ```MainStateMachine.c``` is a main program in C that runs the Bloqqi program a number of periods. After the periods are run, the main program in C calls a generated function that prints coverage statistics.

Run ```make``` to execute the program and print test coverage information.

    $ make
    ...
    state:
       2: S1
       6: S1toS2
       2: S2
       6: S2toS1
       2: S1 => S1toS2: changeState
       2: S1toS2 => S2: counter > delay
       2: S2 => S2toS1: changeState
       1: S2toS1 => S1: counter > delay
       0: S1toS2 => S1: reset
       0: S2 => S1: reset
       0: S2toS1 => S1: reset

In general, statistics for each state machine instance will be printed. Each instance includes information about how many periods each state has been active and how many times each transition has been triggered.
